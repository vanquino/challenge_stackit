import fetch from 'node-fetch';
import * as moment from 'moment';
import 'moment-duration-format';

class TogglService {

    public getWorkspaces( ) : Promise<any> {
        return new Promise((resolve, reject) => {
            // fetch('https://www.toggl.com/api/v8/time_entries', {
                
            // fetch('https://www.toggl.com/api/v8/workspaces/4330393/tasks', {
            //fetch('https://www.toggl.com/api/v8/workspaces/4330393/projects', { OK
            fetch('https://toggl.com/reports/api/v2/summary?workspace_id=4330393&since=2020-08-05&until=2020-08-06&user_agent=victor.anquino@outlook.com', {
                    //https://www.toggl.com/api/v8/time_entries/current
                //https://www.toggl.com/api/v8/time_entries?start_date=2020-01-01&end_date=2020-12-31
                //https://www.toggl.com/api/v8/workspaces/4330393/projects
            // fetch('https://www.toggl.com/api/v8/workspaces/4330393', {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from(
                        'e4fdc768834404305f5b3e341c4bf0cb:api_token'
                    )
                    .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
            .then( result => resolve( result.json() ) )
            .catch( error => reject( error ) )
        });
    }

    public getSummaryByDate(startDate:string, endDate:string,workspace:string, agent:string) : Promise<any> {
        return new Promise((resolve, reject) => {
            fetch(`https://toggl.com/reports/api/v2/summary?workspace_id=${workspace}&since=${startDate}&until=${endDate}&user_agent=${agent}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from(
                        'e4fdc768834404305f5b3e341c4bf0cb:api_token'
                    )
                    .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
            .then( result => resolve( result.json() ) )
            .catch( error => reject( error ) )
        });
    }

    public getDetailsByDate(startDate:string, endDate:string,workspace:string, agent:string) : Promise<any> {
        return new Promise((resolve, reject) => {
            fetch(`https://toggl.com/reports/api/v2/details?workspace_id=${workspace}&since=${startDate}&until=${endDate}&user_agent=${agent}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from(
                        'e4fdc768834404305f5b3e341c4bf0cb:api_token'
                    )
                    .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
            .then( async ( result:any ) => {
                let response = await result.json();
                let entries : [] = response.data;
                let results = entries.map((item:any) => {
                    item = {
                        "id": item.id,
                        "desc": item.description,
                        "start": this.formatTime(item.start),
                        "stop": this.formatTime(item.end),
                        "duration": this.getDurationFormated(item.dur/1000),
                        "durationMillSeconds": item.dur,
                        "tags" : item.tags,
                        "category": this.getCategory(item.tags)
                    }
                    return item
                });
                resolve( results )
            })
            .catch( (error) => {
                reject( error )
            })
        });
    }

    formatTime(time:string) {
        let result = time.split("T")[1];
        return result.split("+")[0];
    }

    getDurationFormated(seconds:number) : string {
        return moment.utc( seconds * 1000 ).format("HH:mm:ss");
    }

    getCategory( tags : [] ) {
        let result = "";

        let categories : any[] = [
            {
                name : "ALPHA",
                tags : [
                    "refactoring",
                    "bug_fix",
                    "test_automation",
                    "testing",
                    "implementation",
                    "refining",
                    "code_review",
                    "deployment",
                    "tech_research",
                    "qa_updates",
                    "qa_testing"
                ]
            },
            {
                name : "OMEGA",
                tags : [
                    "qa_automation",
                    "planning",
                    "design",
                    "estimation",
                    "coordination_work",
                    "process",
                    "training",
                    "pm_admin",
                    "accounting",
                    "payroll",
                    "invoices",
                    "compliance",
                    "admin",
                    "onboarding",
                    "off_boarding",
                    "evaluation",
                    "recruiting",
                    "interviewing"
                ]
            },
            {
                name : "BETA",
                tags : [
                    "_non_billable",
                    "_video_review",
                    "_pair_working",
                    "_meeting"
                ]
            }
        ]

        let isCategory = false;
        tags.forEach( ( tag ) => {
            categories.forEach( category => {
                if( category.name ==  tag ) {
                    isCategory = true;
                    result = category.name;
                } else {
                    if( category.tags.includes(tag) && isCategory == false )
                        result = category.name;
                }
            });
        });

        return result;
    }
}
export const togglService = new TogglService();