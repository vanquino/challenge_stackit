import fetch from 'node-fetch';

class JiraService {

    public getProjectByKey( key : string ) : Promise<any> {
        return new Promise((resolve, reject) => {
            fetch(`https://stackitchallenge.atlassian.net/rest/api/3/project/${ key }`, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from(
                        'victor.anquino@outlook.com:S8I6XjKqdRuvEnRXxshEA956'
                    )
                    .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
            .then( result => resolve( result.json() ) )
            .catch( error => reject( error ) )
        });
    }

    public getProjects() : Promise<any> {
        return new Promise((resolve, reject) => {            
            fetch('https://stackitchallenge.atlassian.net/rest/api/3/project/', {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from(
                        'victor.anquino@outlook.com:S8I6XjKqdRuvEnRXxshEA956'
                    )
                    .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
            .then( result => resolve( result.json() ) )
            .catch( error => reject( error ) )
        });
    }

    public getIssuesByProjectId(projectId : number) : Promise<any> {
        return new Promise((resolve, reject) => {
            fetch(`https://stackitchallenge.atlassian.net/rest/api/2/search?jql=project=${ projectId }`, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from(
                        'victor.anquino@outlook.com:S8I6XjKqdRuvEnRXxshEA956'
                    )
                    .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
            .then( result => resolve( result.json() ) )
            .catch( error => reject( error ) )
        });
    }
}

export const jiraService = new JiraService();