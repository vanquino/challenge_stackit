import { Router } from 'express';
import { challengeController } from '../controllers/challenge_controller';

const router : Router = Router();

router.get('/summary', (req, res) => {
    challengeController.getSummary().then( result => {
        res.send(result);
    }).catch(error => res.status(500).send(error))
});

export default router;