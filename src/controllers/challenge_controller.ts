import fetch from 'node-fetch';
import { jiraService } from './../services/jira_service';
import { togglService } from './../services/toggl_service';
import * as moment from 'moment';
import 'moment-duration-format';

class ChallengeController {
    public async getSummary() {
        try {
            let projects = await jiraService.getProjects();
            if( projects ) {
                let proj_issues = await this.getIssues(projects);
                let date = moment.utc().format("YYYY-MM-DD");
                console.log(date);
                let entries = await togglService.getDetailsByDate(date,date,"4330393","victor.anquino@outlook.com");
                return this.mergeSummary(proj_issues,entries);
            } else {
                return [];
            }
        } catch ( error ) {
            throw error;
        }
    }

    public TimeEntries(summary : any) : any[] {
        let result : any[] = [];
        let projects : any[] = summary.data;
        projects.forEach((project) => {
            let items : any[] = project.items;
            items.forEach((item) => {
                result.push(item);
            })
        });
        return result;
    }

    private getIssues(projects: any[]) : Promise<any> {
        return new Promise((resolve, reject) => {
            let promises : any[] = [];
            projects.forEach( ( project : any ) => {
                let request = jiraService.getIssuesByProjectId(project.id)
                promises.push(request);
                request.then((result) => {
                    project.issues = result.issues;
                })
                promises.push(request);
            });
            Promise.all(promises).then((result) => {
                resolve(projects)
            })
            .catch( error => {
                reject(error);
            });
        });
    }

    private mergeSummary(projects:[], time_entries:any[]) : any[] {
        console.log(1)
        let results : any[] = [];
        projects.forEach((result:any) => {
            console.log(2)
            let project : any = {};
            project.id = result.id;
            project.jiraIssues = [];
            
            result.issues.forEach( ( item:any ) => {
                let issue : any = {};
                let entries = this.getIssueEntries(item.key,time_entries);
                issue.id = item.key;
                issue.statusCategory = item.fields.status.statusCategory.name;
                issue.summary = item.fields.summary;
                issue.assignee = item.fields.assignee ? item.fields.assignee.displayName : "";
                issue.totalDuration = this.getDurationFormated(this.getTotalDuration(entries)/1000);
                issue.totalDurationMillSeconds = this.getTotalDuration(entries);
                issue.aggregatedTags = this.getTags(entries);
                issue.aggregateCategories = this.getCategories(entries);
                console.log("item.fields.timeoriginalestimate",item.fields.timeoriginalestimate);
                issue.estimatedDuration = item.fields.timeoriginalestimate ? this.getDurationFormated(item.fields.timeoriginalestimate) : "00:00:00";
                issue.estimatedDurationMillSeconds =  item.fields.timeoriginalestimate * 1000;
                issue.togglEntries = entries;
                project.jiraIssues.push(issue);
            });

            results.push(project);
        })
        return results;
    }

    getTotalDuration(entries:any[]) {
        let duration = 0;
        entries.forEach((entry:any) => {
            duration += entry.durationMillSeconds;
        });
        return duration;
    }

    getIssueEntries(issue:any, all_entries:any[]) {
        console.log("################ ISUE => ", issue)
        let entries = all_entries.filter( entry => {
            let words = entry.desc.split(" ");
            let belog = false;
            for( let i in words ) {
                if( words[i] ==  issue ) {
                    belog = true;
                    break;
                }
            }
            if( belog )
                return entry
        });
        console.log("################ FIN => ", entries)
        return entries;
    }

    getTags( entries: any[] ) : any[] {
        let result : any[] = [];
        entries.forEach( ( entry : any ) => {
            entry.tags.forEach( ( tag:any ) => {
                result.push(tag);
            })
        });
        return result;
    }

    getCategories( entries: any[] ) : any[] {
        let result : any[] = [];
        entries.forEach( ( entry : any ) => {
            result.push( entry.category )
        });
        return result;
    }

    getDurationFormated(seconds:number) : string {
        return moment.utc( seconds * 1000 ).format("HH:mm:ss");
    }
}

export const challengeController = new ChallengeController();

/*
{
    "projectID": 926763402
    "jiraIssues": [
        {
          "id": "PA-2",
          "statusCategory": "Done",
          "summary": "PA-2 - Emptied repo.",
          "assignee": "Joe Doe",
          "totalDuration": "00:01:00",
          "totalDurationMillSeconds": 60000,
          "aggregatedTags": [],
          "aggregateCategories": [],
          "estimatedDuration": "03:00:00",
          "estimatedDurationMillSeconds": 10000000
          "togglEntries": [
            "id": 1233465,
            "desc": "Worked on the task PA-2",
            "start": "15:30:52",
            "stop": "16:00:52",
            "duration": "00:30:00",
            "durationMillSeconds": 1800000
            "tags" : ["_meeting", "process"], 
            "category": "OMEGA";
          ]
        },
    ]
  }

  */